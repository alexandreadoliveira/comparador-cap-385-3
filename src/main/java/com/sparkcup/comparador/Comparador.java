package com.sparkcup.comparador;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alexandre
 */
public class Comparador {
    
    public static <E> List<Diferenca> comparar(E velho, E novo) throws Exception, IllegalArgumentException {
        List<Diferenca> difs = new ArrayList<>();
        
        Class<?> clazz = velho.getClass();
        
        for(Method m : clazz.getMethods()) {
            if(m.getName().startsWith("get") && 
                    m.getParameterCount() == 0 && 
                    m.getReturnType() != void.class &&
                    m.isAnnotationPresent(IgnorarNaComparacao.class)) {
                Object valorNovo = m.invoke(novo);
                Object valorVelho = m.invoke(velho);
                if(valorNovo != null) {
                    if(valorVelho != null && m.isAnnotationPresent(PodeEntrar.class)) {
                        List<Diferenca> difsProp = comparar(valorVelho, valorNovo);
                        for(Diferenca dif : difsProp) {
                            dif.setPropriedade(m.getName() + "." + dif.getPropriedade());
                            difs.add(dif);
                        }
                    } else if(!valorNovo.equals(valorVelho)) {
                        difs.add(new Diferenca(m.getName(), valorNovo, valorVelho));
                    }
                } else {
                    if(valorVelho != null) {
                        difs.add(new Diferenca(m.getName(), valorNovo, valorVelho));
                    }
                }
            }
        }
        
        return difs;
    }
    
}
