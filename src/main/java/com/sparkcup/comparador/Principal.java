package com.sparkcup.comparador;

import java.util.List;

/**
 *
 * @author alexandre
 */
public class Principal {
    
    public static void main(String args[]) throws Exception {
        Pessoa velho = new Pessoa("Alexandre", "Oliveira", 28, "Tecnico");
        velho.setEndereco(new Endereco("Rua 1", "2A", "RJ", "Cachoeira Paulista"));
        
        Pessoa novo = new Pessoa("Alexandre", "Oliveira", 29, "Tecnologista");
        novo.setEndereco(new Endereco("Rua 2", "2B", "SP", "Paulista"));
        
        List<Diferenca> list = Comparador.comparar(velho, novo);
        
        list.forEach(System.out::println);
    }
    
}
