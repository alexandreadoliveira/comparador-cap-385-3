package com.sparkcup.comparador;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author alexandre
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface PodeEntrar {
    
}
